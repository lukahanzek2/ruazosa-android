package hr.fer.ruazosa.timesheetapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.ruazosa.timesheetapp.entity.Task
import hr.fer.ruazosa.timesheetapp.utilities.DateUtilities.makeDateToString
import java.util.*
import kotlin.collections.ArrayList

class TaskAdapter (private val mList: List<Task>, private val mOnTaskListener: RecyclerViewOnClickListener, private val mNoItemsCallback: NoItemsCallback) : RecyclerView.Adapter<TaskAdapter.ViewHolderTask>(){
    var dataSet:ArrayList<Task> = ArrayList(mList)
    var dateFiltered:ArrayList<Task> = ArrayList(mList)
    var textFiltered:ArrayList<Task> = ArrayList(mList)

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTask {// inflates the card_view_design view
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
                //tu je potrebno inflate-ati layout za taskove, a ne za projekte
            .inflate(R.layout.task_view_design, parent, false)

        return ViewHolderTask(view, mOnTaskListener)
    }
    // binds the list items to a view
    override fun onBindViewHolder(holder: TaskAdapter.ViewHolderTask, position: Int) {
        val ItemsViewModel = dataSet[position]

        // sets the text to the textview from our itemHolder class
        holder.name.text = ItemsViewModel.taskName

        // sets the text to the textview from our itemHolder class
        holder.date.text = makeDateToString(ItemsViewModel.date)

        // sets the text to the textview from our itemHolder class
        holder.hours.text = ItemsViewModel.hours.toString()
    }


    // return the number of the items in the list
    override fun getItemCount(): Int {
        return dataSet.size
    }

    class ViewHolderTask(ItemView: View, onTaskListener: RecyclerViewOnClickListener) : RecyclerView.ViewHolder(ItemView), View.OnClickListener {
        var onTaskListener: RecyclerViewOnClickListener= onTaskListener
        val name: TextView = itemView.findViewById((R.id.taskName))
        val date: TextView = itemView.findViewById(R.id.taskDate)
        val hours: TextView = itemView.findViewById(R.id.taskHours)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onTaskListener.onClick(adapterPosition)
        }
    }

    fun getFilterDate(): Filter {
        return Selected_Date_Filter
    }

    fun getFilterText(): Filter{
        return Search_Filter
    }

    private val Selected_Date_Filter : Filter = object : Filter (){
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            dateFiltered.clear()

            val selectedDate = Date(constraint.toString())
            for(item in mList){
                if(item.date!!.equals(selectedDate)){
                    dateFiltered.add(item)
                }
            }
            val results = FilterResults()
            results.values = dateFiltered
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            dataSet.clear()

            val resultsList = (results.values as ArrayList<Task>)
            if(results == null){
                mNoItemsCallback.onRecyclerViewRefresh(true)
            }
            else {
                mNoItemsCallback.onRecyclerViewRefresh(false)
                dataSet.addAll(ArrayList(resultsList.intersect(textFiltered)))

            }
            if(dataSet.isEmpty()){
                mNoItemsCallback.onRecyclerViewRefresh(true)
            }
            notifyDataSetChanged()
        }
    }

    private val Search_Filter : Filter = object : Filter (){
        override fun performFiltering(constraint: CharSequence): FilterResults {
            textFiltered.clear()

            val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
            for (item in mList) {
                if (item.taskName.toLowerCase().contains(filterPattern)) {
                    textFiltered.add(item)
                }
            }

            val results = FilterResults()
            results.values = textFiltered
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            dataSet.clear()

            val resultsList = (results.values as ArrayList<Task>)
            if(results == null){
                mNoItemsCallback.onRecyclerViewRefresh(true)
            }
            else {
                mNoItemsCallback.onRecyclerViewRefresh(false)
                dataSet.addAll(ArrayList(resultsList.intersect(dateFiltered)))

            }
            if(dataSet.isEmpty()){
                mNoItemsCallback.onRecyclerViewRefresh(true)
            }
            notifyDataSetChanged()
        }
    }

}