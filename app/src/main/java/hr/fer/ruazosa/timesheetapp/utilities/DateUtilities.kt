package hr.fer.ruazosa.timesheetapp.utilities

import java.util.*

object DateUtilities {

    public fun makeDateToString(date: Date?): String {
        if(date != null){
            return getMonthFormat(date.month) + " " + date.date + " " + (date.year + 1900).toString()
        }
        return ""
    }

    private fun getMonthFormat(month: Int): String {
        when(month){
            0 -> return "JAN"
            1 -> return "FEB"
            2 -> return "MAR"
            3 -> return "APR"
            4 -> return "MAY"
            5 -> return "JUN"
            6 -> return "JUL"
            7 -> return "AUG"
            8 -> return "SEP"
            9 -> return "OCT"
            10 -> return "NOV"
            11 -> return "DEC"
            else -> return "---" //do ovog ne bi trebalo doci
        }
    }

}