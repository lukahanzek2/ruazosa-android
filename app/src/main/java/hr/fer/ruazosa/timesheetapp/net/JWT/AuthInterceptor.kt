package hr.fer.ruazosa.timesheetapp.net.JWT

import android.content.Context
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(context: Context) : Interceptor {
    private val userRepo = UserRepository(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        // If token has been saved, add it to the request
        userRepo.fetchAuthToken()?.let {
            requestBuilder.addHeader("Authorization", "Bearer $it")
        }

        return chain.proceed(requestBuilder.build())
    }
}