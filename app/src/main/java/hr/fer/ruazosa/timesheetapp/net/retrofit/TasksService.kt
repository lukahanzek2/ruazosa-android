package hr.fer.ruazosa.timesheetapp.net.retrofit

import hr.fer.ruazosa.timesheetapp.entity.Task
import retrofit2.Call
import retrofit2.http.*

interface TasksService {
    @GET("/tasks/{projectId}/{userName}")
    fun listOfTasks(@Path("projectId") projectId: Long, @Path("userName") userName: String): Call<List<Task>>

    @POST("/addTask/{userName}/{projectId}") //mozda treba prepraviti path
    fun addTask(@Body newTask: Task, @Path ("projectId")projectId: Long, @Path ("userName")userName: String): Call<Task>

    @PUT("/editTask/{taskId}")
    fun editTask(@Body newTask: Task, @Path ("taskId")taskId: Long): Call<Task>
}