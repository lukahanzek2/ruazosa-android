package hr.fer.ruazosa.timesheetapp.auth

import android.app.AlertDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import hr.fer.ruazosa.timesheetapp.ProjectsActivity
import hr.fer.ruazosa.timesheetapp.R
import hr.fer.ruazosa.timesheetapp.databinding.SignUpFragmentBinding
import hr.fer.ruazosa.timesheetapp.entity.Person
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository


class SignUpFragment : Fragment(R.layout.sign_up_fragment) {

    private lateinit var binding: SignUpFragmentBinding;

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = SignUpFragmentBinding.bind(view);

        usernameFocusListener()
        firstNameFocusListener()
        lastNameFocusListener()
        emailFocusListener()
        passwordFocusListener()
        confirmedPasswordFocusListener()

        binding.registerId.setOnClickListener {
            signUp()
        }
    }

    fun registerUser(user: Person){
        RegisterUserTask().execute(user)
    }

    private inner class RegisterUserTask: AsyncTask<Person, Void, Pair<Person?, String?>>() {
        override fun doInBackground(vararg params: Person): Pair<Person?, String?> {
            val rest = RestFactory.getInstance(context!!.applicationContext)
            return rest.registerUser(params[0])
        }

        override fun onPostExecute(registerData: Pair<Person?, String?>) {
            val registeredUser = registerData.first
            val message = registerData.second
            if(registeredUser == null){
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                activity!!.supportFragmentManager.popBackStack("register", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }
    }

    private fun signUp() {
        binding.firstNameId.helperText = validFirstName()
        binding.lastNameId.helperText = validLastName()
        binding.emailId.helperText = validEmail()
        binding.passwordId.helperText = validPassword()
        binding.confirmedPasswordId.helperText = validConfirmedPassword()

        val validFirstName = binding.firstNameId.helperText == null
        val validLastName = binding.lastNameId.helperText == null
        val validEmail = binding.emailId.helperText == null
        val validPassword = binding.passwordId.helperText == null
        val validConfirmedPassword = binding.confirmedPasswordId.helperText == null

        if (validFirstName && validLastName && validEmail && validPassword && validConfirmedPassword){
            val username = binding.usernameEditId.text.toString()
            val firstName = binding.firstNameEditTextId.text.toString()
            val lastName = binding.lastNameEditTextId.text.toString()
            val email = binding.emailEditTextId.text.toString()
            val password = binding.passwordEditTextId.text.toString()
            val newUser: Person = Person(null, username, firstName, lastName, email, password)
            registerUser(newUser)
        }else {
            invalidSignUp()
        }
    }


    private fun invalidSignUp(){
        var message = ""
        if(binding.firstNameId.helperText != null)
            message += "\n\nFirst Name: " + binding.firstNameId.helperText
        if(binding.lastNameId.helperText != null)
            message += "\n\nLast Name: " + binding.lastNameId.helperText
        if(binding.emailId.helperText != null)
            message += "\n\nEmail: " + binding.emailId.helperText
        if(binding.passwordId.helperText != null)
            message += "\n\nPassword: " + binding.passwordId.helperText
        if(binding.confirmedPasswordId.helperText != null)
            message += "\n\nConfirmed Password: " + binding.confirmedPasswordId.helperText

        AlertDialog.Builder(context)
            .setTitle("Invalid Sign Up")
            .setMessage(message)
            .setPositiveButton("Okay"){ _,_ ->
                // do nothing
            }
            .show()

    }

    private fun checkUser(){
        //TO DO: provjera sa DB jel username unique?
    }

    private fun usernameFocusListener(){
        binding.usernameEditId.setOnFocusChangeListener{_, focused ->
            if(!focused)
            {
                binding.usernameId.helperText = validUsername()
            }
        }
    }

    private fun validUsername(): String?{
        val username = binding.usernameEditId.text.toString()
        if(username == null || username == ""){
            return "required"
        }

        return ""
    }

    private fun firstNameFocusListener(){
        binding.firstNameEditTextId.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.firstNameId.helperText = validFirstName()
            }
        }
    }

    private fun validFirstName(): String?
    {
        val firstNameText = binding.firstNameEditTextId.text.toString()
        if(firstNameText == null || firstNameText == ""){
            return "required"
        }
        if(!firstNameText[0].isUpperCase()){
            return "Must start with uppercase letter"
        }
        if(!firstNameText.matches("[A-Z][a-z]*".toRegex()))
        {
            return "Must be all letters"
        }
        return null
    }

    private fun lastNameFocusListener(){
        binding.lastNameEditTextId.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.lastNameId.helperText = validLastName()
            }
        }
    }

    private fun validLastName(): String?
    {
        val lastNameText = binding.lastNameEditTextId.text.toString()
        if(lastNameText == null || lastNameText == ""){
            return "required"
        }
        if(!lastNameText[0].isUpperCase()){
            return "Must start with uppercase letter"
        }
        if(!lastNameText.matches("[A-Z][a-z]*".toRegex()))
        {
            return "Must be all letters"
        }
        return null
    }

    private fun emailFocusListener()
    {
        binding.emailEditTextId.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.emailId.helperText = validEmail()
            }
        }
    }

    private fun validEmail(): String?
    {
        val emailText = binding.emailEditTextId.text.toString()
        if(emailText == "" || emailText == null){
            return "required"
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(emailText).matches())
        {
            return "Invalid Email Address"
        }
        return null
    }

    private fun passwordFocusListener()
    {
        binding.passwordEditTextId.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.passwordId.helperText = validPassword()
            }
        }
    }

    private fun validPassword(): String?
    {
        val passwordText = binding.passwordEditTextId.text.toString()
        if(passwordText.length < 8)
        {
            return "Minimum 8 Character Password"
        }
        if(!passwordText.matches(".*[A-Z].*".toRegex()))
        {
            return "Must Contain 1 Upper-case Character"
        }
        if(!passwordText.matches(".*[a-z].*".toRegex()))
        {
            return "Must Contain 1 Lower-case Character"
        }
        if(!passwordText.matches(".*[0-9].*".toRegex()))
        {
            return "Must contain 1 Digit"
        }
        if(!passwordText.matches(".*[@#\$%^&+=].*".toRegex()))
        {
            return "Must Contain 1 Special Character (@#\$%^&+=)"
        }

        return null
    }

    private fun confirmedPasswordFocusListener()
    {
        binding.confirmedPasswordEditTextId.setOnFocusChangeListener { _, focused ->
            if(!focused)
            {
                binding.confirmedPasswordId.helperText = validConfirmedPassword()
            }
        }
    }

    private fun validConfirmedPassword(): String?
    {
        val confirmedPasswordText = binding.confirmedPasswordEditTextId.text.toString()
        val passwordText = binding.passwordEditTextId.text.toString()
        if(!confirmedPasswordText.equals(passwordText))
        {
            return "Password Not Matching"
        }
        return null
    }
}