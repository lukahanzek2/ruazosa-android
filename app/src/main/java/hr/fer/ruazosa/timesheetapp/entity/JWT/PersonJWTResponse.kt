package hr.fer.ruazosa.timesheetapp.entity.JWT

import hr.fer.ruazosa.timesheetapp.entity.Person

data class PersonJWTResponse (
    var statusCode: Int,
    var token : String?,
    var user: Person? = null
    /*val id: Long? = null,
    val username: String?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val password: String?*/
)
