package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable

data class Person(
    val id: Long? = null,
    val username: String?,
    val firstName: String?,
    val lastName: String?,
    val email: String?,
    val password: String?
) : Serializable