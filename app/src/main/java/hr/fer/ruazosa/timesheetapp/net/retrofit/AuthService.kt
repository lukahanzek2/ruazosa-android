package hr.fer.ruazosa.timesheetapp.net.retrofit

import hr.fer.ruazosa.timesheetapp.entity.JWT.PersonJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.Person
import hr.fer.ruazosa.timesheetapp.entity.ShortPerson
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {
    @POST("/registerUser")
    fun registerUser(@Body user: Person): Call<Person>

    @POST("/authenticate")
    fun loginUser(@Body user: ShortPerson): Call<PersonJWTResponse>
}