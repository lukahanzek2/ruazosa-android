package hr.fer.ruazosa.timesheetapp.net

import hr.fer.ruazosa.timesheetapp.entity.*
import hr.fer.ruazosa.timesheetapp.entity.JWT.EmptyJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.PersonJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.ShortProjectListJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.TaskJWTResponse

interface RestInterface {

    //AuthService
    fun loginUser(user: ShortPerson): Pair<PersonJWTResponse?, String?>
    fun registerUser(user: Person): Pair<Person?, String?>

    //ProjectsService
    fun getlistOfProjects(username: String?): Pair<ShortProjectListJWTResponse?, String?>

    //TasksSevrice
    fun getlistOfTasks(projectId: Long, username: String): Pair<TaskJWTResponse?, String?>
    fun addTask(newTask:Task, projectId: Long, userName: String) : Pair<EmptyJWTResponse?, String?>
    fun editTask(newTask: Task) : Pair<EmptyJWTResponse?, String?>
}

