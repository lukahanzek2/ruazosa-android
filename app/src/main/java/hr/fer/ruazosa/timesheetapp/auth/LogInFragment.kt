package hr.fer.ruazosa.timesheetapp.auth

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import hr.fer.ruazosa.timesheetapp.ProjectsActivity
import hr.fer.ruazosa.timesheetapp.R
import hr.fer.ruazosa.timesheetapp.databinding.LogInFragmentBinding
import hr.fer.ruazosa.timesheetapp.entity.JWT.PersonJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.ShortPerson
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository

class LogInFragment : Fragment(R.layout.log_in_fragment) {

    private lateinit var binding: LogInFragmentBinding;

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = LogInFragmentBinding.bind(view);

        val signUpFragment = SignUpFragment();

        //TO DO:
        //-Validacijska logika (validacijska pravila)
        //-Poziv REST usluge za registraciju
        //-Logiku provjere rezultata izvršavanja REST usluge

        binding.btnLogin.setOnClickListener{
            val username = binding.tfUserName.text.toString()
            val password = binding.tfPassword.text.toString()

            val shortPerson = ShortPerson(username, password)

            //to do: provjera unesenih podataka, ako su valjani -> ProjectsActivity

            loginUser(shortPerson)
        }

        binding.btnSignUp.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(requireActivity().findViewById<FrameLayout>(R.id.flFragment).id, signUpFragment);
                addToBackStack("register")
                commit()
            }
        }
    }

    private fun loginUser(user: ShortPerson){
        RegisterUserTask().execute(user)
    }

    private inner class RegisterUserTask: AsyncTask<ShortPerson, Void, Pair<PersonJWTResponse?, String?>>() {
        override fun doInBackground(vararg params: ShortPerson): Pair<PersonJWTResponse?, String?> {
            val rest = RestFactory.getInstance(context!!)
            return rest.loginUser(params[0])
        }

        override fun onPostExecute(response: Pair<PersonJWTResponse?, String?>) {
            val jwtResponse = response.first
            val message = response.second
            //val person = Person(jwtResponse?.id, jwtResponse?.username, jwtResponse?.firstName, jwtResponse?.lastName, jwtResponse?.email,jwtResponse?.password)
            if(jwtResponse?.statusCode == 401){
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                UserRepository(context!!).clearPreferences()
                val intent = Intent(context, AuthActivity::class.java)
                startActivity(intent)
            }
            if(jwtResponse == null || jwtResponse.user == null){
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            } else{
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                //store user in local repository
                UserRepository(context!!).setUserloggedIn(true)
                UserRepository(context!!).storeUserData(jwtResponse.user!!)
                //store token
                UserRepository(context!!).saveAuthToken(jwtResponse.token!!)
                val intent = Intent(context, ProjectsActivity::class.java)
                startActivity(intent)
            }
        }
    }
}