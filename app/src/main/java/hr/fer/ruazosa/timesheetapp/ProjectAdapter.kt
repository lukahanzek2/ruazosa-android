package hr.fer.ruazosa.timesheetapp

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.fer.ruazosa.timesheetapp.entity.ShortProject
import android.view.LayoutInflater
import android.view.View
import android.widget.Filter
import android.widget.Filter.FilterResults
import android.widget.Filterable
import android.widget.TextView


class ProjectAdapter(private val mList: List<ShortProject>, private val mOnProjectListener: RecyclerViewOnClickListener, private val mNoItemsListener: NoItemsCallback) : RecyclerView.Adapter<ProjectAdapter.ViewHolderProject>(), Filterable{

    var dataSet:ArrayList<ShortProject> = ArrayList(mList)
    val completeList:ArrayList<ShortProject> = ArrayList(mList)
    //context - ProjectsActivity kojega referencira ViewHolder u OnClock metodi

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectAdapter.ViewHolderProject {// inflates the card_view_design view
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.project_view_design, parent, false)

        return ViewHolderProject(view, mOnProjectListener)
    }
    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolderProject, position: Int) {

        val ItemsViewModel = dataSet[position]

        // sets the text to the textview from our itemHolder class
        holder.name.text = ItemsViewModel.projectName

        // sets the text to the textview from our itemHolder class
        holder.hours.text = ItemsViewModel.hours.toString()

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return dataSet.size
    }

    inner class ViewHolderProject(ItemView: View, mOnProjectListener: RecyclerViewOnClickListener) :View.OnClickListener, RecyclerView.ViewHolder(ItemView) {
        var onProjectListener: RecyclerViewOnClickListener = mOnProjectListener
        val name: TextView = itemView.findViewById((R.id.projectName))
        val hours: TextView = itemView.findViewById(R.id.projectHours)

        init{
            ItemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onProjectListener.onClick(adapterPosition)
        }
    }

    override fun getFilter(): Filter {
        return Searched_Filter
    }

    private val Searched_Filter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            val filteredList: ArrayList<ShortProject> = ArrayList()
            if (constraint == null || constraint.length == 0) {
                filteredList.addAll(completeList)
            } else {
                val filterPattern = constraint.toString().toLowerCase().trim { it <= ' ' }
                for (item in completeList) {
                    if (item.projectName.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            if(results == null || (results.values as ArrayList<ShortProject>).isEmpty()){
                mNoItemsListener.onRecyclerViewRefresh(true)
            }else{
                mNoItemsListener.onRecyclerViewRefresh(false)
            }
            dataSet.clear()
            dataSet.addAll(results.values as ArrayList<ShortProject>)
            notifyDataSetChanged()

        }
    }

}