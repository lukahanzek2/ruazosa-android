package hr.fer.ruazosa.timesheetapp.net

import android.content.Context
import hr.fer.ruazosa.timesheetapp.R
import hr.fer.ruazosa.timesheetapp.net.retrofit.RestRetrofit

object RestFactory {
    val BASE_IP = "timesheet-0-lukahanzek-dev.apps.sandbox-m2.ll9k.p1.openshiftapps.com/"

    var instance: RestInterface? = null

    fun getInstance(context: Context) : RestInterface {
        if(instance == null){
            instance = RestRetrofit(context)
        }

        return instance!!
    }

}