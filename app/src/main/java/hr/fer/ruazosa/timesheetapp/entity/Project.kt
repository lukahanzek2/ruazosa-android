package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable

data class Project(
    var id: Long? = null,
    var projectName: String = "",
    var tasks: MutableList<Task> = ArrayList<Task>(),
) : Serializable