package hr.fer.ruazosa.timesheetapp.net.retrofit

import android.content.Context
import com.google.gson.GsonBuilder
import hr.fer.ruazosa.timesheetapp.entity.*
import hr.fer.ruazosa.timesheetapp.entity.JWT.EmptyJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.PersonJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.ShortProjectListJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.JWT.TaskJWTResponse
import hr.fer.ruazosa.timesheetapp.net.JWT.AuthInterceptor
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.net.RestInterface
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class RestRetrofit(private val context: Context) : RestInterface {
    private val projects: ProjectsService
    private val tasks: TasksService
    private val auth: AuthService

    init {
        val gsonBuilder = GsonBuilder()
        val gson = gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
        val baseURL = "https://" + RestFactory.BASE_IP // + ":8080/"
        val retrofit = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okhttpClient(context))
            .build()
        projects = retrofit.create(ProjectsService::class.java)
        tasks = retrofit.create(TasksService::class.java)
        auth = retrofit.create(AuthService::class.java)
    }

    /**
     * Initialize OkhttpClient with our interceptor
     */
    private fun okhttpClient(context: Context): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(AuthInterceptor(context))
            .build()
    }

    override fun getlistOfProjects(username: String?): Pair<ShortProjectListJWTResponse?, String?>{
        val call = projects.listOfProjects(username)
        try {
            val response = call.execute()
            val jwtResponse = ShortProjectListJWTResponse(response.code(), null)
            if(!response.isSuccessful){
                return Pair(jwtResponse, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            jwtResponse.shortProjectList = response.body()!!
            return Pair(jwtResponse, null) //mislim da tu ne treba poruka
        } catch (e: IOException){
            return Pair(null, e.message)
        } catch (e: java.lang.RuntimeException){
            return  Pair(null, e.message)
        }
    }
    override fun getlistOfTasks(projectId: Long, userName: String): Pair<TaskJWTResponse?, String?>{
        val call = tasks.listOfTasks(projectId, userName)
        try{
            val response = call.execute()
            val jwtResponse = TaskJWTResponse(response.code(), null)
            if(!response.isSuccessful){
                return Pair(jwtResponse, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            jwtResponse.tasksList = response.body()!!
            return Pair(jwtResponse, null) //mislim da tu ne treba poruka
        } catch (e: IOException){
            return Pair(null, e.message)
        } catch (e: java.lang.RuntimeException){
            return Pair(null, e.message)
        }
    }

    override fun addTask(newTask : Task, projectId: Long, userName: String): Pair<EmptyJWTResponse?, String?> {
        val call = tasks.addTask(newTask, projectId, userName)
        try{
            val response = call.execute()
            val jwtResponse = EmptyJWTResponse(response.code())
            if(!response.isSuccessful){
                return Pair(jwtResponse, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            return Pair(jwtResponse, "Task added")
        } catch (e: IOException){
            return Pair(null, e.message)
        } catch (e: java.lang.RuntimeException){
            return Pair(null, e.message)
        }
    }

    override fun editTask(newTask: Task): Pair<EmptyJWTResponse?, String?> {
        val call = tasks.editTask(newTask, newTask.id!!)
        try{
            val response = call.execute()
            val jwtResponse = EmptyJWTResponse(response.code())
            if(!response.isSuccessful){
                return Pair(jwtResponse, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            return Pair(jwtResponse, "Task edited")
        } catch (e: IOException){
            return Pair(null, e.message)
        } catch (e: java.lang.RuntimeException){
            return Pair(null, e.message)
        }
    }

    override fun loginUser(user: ShortPerson): Pair<PersonJWTResponse?, String?> {
        val call = auth.loginUser(user)
        try {
            val response = call.execute()
            val jwtResponse = PersonJWTResponse(response.code(), null, null)
            if(!response.isSuccessful){
                return Pair(jwtResponse, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            jwtResponse.token = response.body()!!.token
            jwtResponse.user = response.body()!!.user
            return Pair(jwtResponse, "Login successful")
        }catch (e: IOException){
            return Pair(null, e.message)
        }catch (e: RuntimeException){
            return Pair(null, e.message)
        }
    }

    override fun registerUser(user: Person): Pair<Person?, String?> {
        val call = auth.registerUser(user)
        try{
            val response = call.execute()
            if(!response.isSuccessful){
                return Pair(null, response.errorBody()?.string().toString().removeSurrounding("{", "}"))
            }
            return Pair(response.body(), "Register successful")
        }catch (e: IOException){
            return Pair(null, e.message)
        }catch (e: RuntimeException){
            return Pair(null, e.message)
        }
    }
}