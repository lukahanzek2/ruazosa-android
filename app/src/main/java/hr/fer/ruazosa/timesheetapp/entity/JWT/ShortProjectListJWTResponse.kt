package hr.fer.ruazosa.timesheetapp.entity.JWT

import hr.fer.ruazosa.timesheetapp.entity.Person
import hr.fer.ruazosa.timesheetapp.entity.ShortProject

data class ShortProjectListJWTResponse (
    var statusCode: Int,
    var shortProjectList: List<ShortProject>?= null
)