package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable

data class ShortTask(
    var taskName: String = "",
    var hours: Int? = null,
) : Serializable {
    constructor(t: Task) : this(
        t.taskName,
        t.hours
    )
}