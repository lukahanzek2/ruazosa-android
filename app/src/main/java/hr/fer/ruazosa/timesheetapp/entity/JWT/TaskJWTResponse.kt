package hr.fer.ruazosa.timesheetapp.entity.JWT

import hr.fer.ruazosa.timesheetapp.entity.Person
import hr.fer.ruazosa.timesheetapp.entity.Task

data class TaskJWTResponse (
    var statusCode: Int,
    var tasksList: List<Task>? = null
)
