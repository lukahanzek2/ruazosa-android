package hr.fer.ruazosa.timesheetapp

interface RecyclerViewOnClickListener {
    fun onClick(position: Int)
}