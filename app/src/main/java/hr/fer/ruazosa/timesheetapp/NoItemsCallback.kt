package hr.fer.ruazosa.timesheetapp

interface NoItemsCallback {
    fun onRecyclerViewRefresh(visible: Boolean)
}