package hr.fer.ruazosa.timesheetapp

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.ruazosa.timesheetapp.auth.AuthActivity
import hr.fer.ruazosa.timesheetapp.databinding.ProjectsActivityBinding
import hr.fer.ruazosa.timesheetapp.entity.JWT.ShortProjectListJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.ShortProject
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository
import kotlinx.android.synthetic.main.projects_activity.*


class ProjectsActivity : AppCompatActivity(), RecyclerViewOnClickListener, NoItemsCallback{
    private lateinit var binding: ProjectsActivityBinding
    private lateinit var adapter: ProjectAdapter
    private var textFilter: String = ""

    override fun onRestart() {
        super.onRestart()

        LoadProjectsTask().execute(UserRepository(applicationContext).getLoggedInUser().username)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ProjectsActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)


        buildRecyclerView()

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
    private fun buildRecyclerView() {
        // this creates a vertical layout Manager
        binding.projectListViewId.layoutManager = LinearLayoutManager(this)

        val username = UserRepository(applicationContext).getLoggedInUser().username
        LoadProjectsTask().execute(username)
    }

    private inner class LoadProjectsTask: AsyncTask<String, Void,  Pair<ShortProjectListJWTResponse?, String?>>() {
        override fun doInBackground(vararg params: String):  Pair<ShortProjectListJWTResponse?, String?> {
            val rest = RestFactory.getInstance(applicationContext)
            return rest.getlistOfProjects(params[0])
        }

        override fun onPostExecute(returnedData:  Pair<ShortProjectListJWTResponse?, String?>) {
            var jwtResponse = returnedData.first
            val message = returnedData.second
            if(jwtResponse?.statusCode == 401){
                Toast.makeText(applicationContext, "Session expired", Toast.LENGTH_LONG).show()
                UserRepository(applicationContext).clearPreferences()
                val intent = Intent(applicationContext, AuthActivity::class.java)
                startActivity(intent)
            }
            if(jwtResponse == null || jwtResponse.shortProjectList == null){
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            } else{
                if(jwtResponse.shortProjectList!![0] == null) {
                    binding.EmptyRecyclerViewText.visibility = View.VISIBLE
                    jwtResponse.shortProjectList = ArrayList<ShortProject>()
                }
                adapter = ProjectAdapter(jwtResponse.shortProjectList!!, this@ProjectsActivity, this@ProjectsActivity)
                binding.projectListViewId.adapter = adapter
                adapter.filter.filter(textFilter)
            }
        }
    }

    override fun onClick(position: Int) {
        val intent = Intent(applicationContext, TaskEnterActivity::class.java)
        intent.putExtra("SHORT_PROJECT", adapter.dataSet[position])
        startActivity(intent)
    }


    override fun onRecyclerViewRefresh(visible: Boolean){
        if(visible){
            binding.EmptyRecyclerViewText.visibility = View.VISIBLE
        }else{
            binding.EmptyRecyclerViewText.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater = menuInflater
        inflater.inflate(R.menu.menu_icons, menu)

        val searchItem = menu?.findItem(R.id.actionSearch)
        val searchView = searchItem?.actionView as SearchView


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                searchView.setQuery("", false)
                searchItem.collapseActionView()
                Toast.makeText(this@ProjectsActivity, "$query", Toast.LENGTH_LONG).show()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                textFilter = newText?:""
                adapter.filter.filter(textFilter)

                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var itemview = item.itemId

        when(itemview){
            R.id.LogoutBtn -> {
                val logoutIntent = Intent(this, AuthActivity::class.java)
                logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                UserRepository(applicationContext).clearPreferences()  //logout -> izbisi user-a iz lokalne baze
                startActivity(logoutIntent)
            }
        }
        return false
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
