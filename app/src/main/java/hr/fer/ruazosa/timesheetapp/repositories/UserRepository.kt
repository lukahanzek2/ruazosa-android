package hr.fer.ruazosa.timesheetapp.repositories

import android.content.Context
import android.content.SharedPreferences
import hr.fer.ruazosa.timesheetapp.R
import hr.fer.ruazosa.timesheetapp.entity.Person

class UserRepository(context: Context) {
    companion object{
        const val USER_TOKEN = "user_token"
    }

    private var prefs: SharedPreferences


    init{
        prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)
    }
    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String) {
        val spEditor = prefs.edit()
        spEditor.putString(UserRepository.USER_TOKEN, token)
        spEditor.commit()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String? {
        return prefs.getString(UserRepository.USER_TOKEN, null)
    }


    fun storeUserData(user: Person){
        val spEditor: SharedPreferences.Editor = prefs.edit()
        spEditor.putString("username", user.username)
        spEditor.putString("firstName", user.firstName)
        spEditor.putString("lastName", user.lastName)
        spEditor.putString("email", user.email)
        spEditor.putString("password", user.password)

        spEditor.commit()
    }

    fun getLoggedInUser(): Person{
        val username = prefs.getString("username", "")
        val firstname = prefs.getString("firstName", "")
        val lastName = prefs.getString("lastName", "")
        val email = prefs.getString("email", "")
        val password = prefs.getString("password", "")

        val storedUser = Person(null, username!!, firstname!!, lastName!!, email!!, password!!)
        return storedUser
    }

    fun setUserloggedIn(loggedIn: Boolean){
        val spEditor: SharedPreferences.Editor = prefs.edit()
        spEditor.putBoolean("loggedIn", loggedIn)

        spEditor.commit()
    }

    fun getUserLoggedIn() : Boolean{
        return prefs.getBoolean("loggedIn", false)
    }

    fun clearPreferences(){
        val spEditor: SharedPreferences.Editor = prefs.edit()
        spEditor.clear()
        spEditor.commit()
    }
}