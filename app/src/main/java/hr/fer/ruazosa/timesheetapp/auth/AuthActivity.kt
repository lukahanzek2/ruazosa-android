package hr.fer.ruazosa.timesheetapp.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hr.fer.ruazosa.timesheetapp.ProjectsActivity
import hr.fer.ruazosa.timesheetapp.databinding.ActivityAuthBinding
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository


class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater);
        setContentView(binding.root)

        val logInFragment = LogInFragment()

        if(UserRepository(applicationContext).getUserLoggedIn()){
            val intent = Intent(applicationContext, ProjectsActivity::class.java)
            startActivity(intent)
            finish()
        }
        //inicijalno uvijek postavljamo log in fragment
        supportFragmentManager.beginTransaction().apply {
            replace(binding.flFragment.id, logInFragment, "LOGIN_FRAGMENT") //tag fragment for back button action
            commit()
        }

    }

    //if log_in_fragment is currently displayed, back button exits application, otherwise pop backstack
    override fun onBackPressed() {
        val currentFragmentTag = supportFragmentManager.fragments.last()?.tag
        if(currentFragmentTag != null && currentFragmentTag == "LOGIN_FRAGMENT"){
            finish()
        }else{
            super.onBackPressed()
        }
    }
}

