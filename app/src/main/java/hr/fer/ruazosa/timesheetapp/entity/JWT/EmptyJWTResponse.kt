package hr.fer.ruazosa.timesheetapp.entity.JWT

data class EmptyJWTResponse (
    var statusCode: Int
)