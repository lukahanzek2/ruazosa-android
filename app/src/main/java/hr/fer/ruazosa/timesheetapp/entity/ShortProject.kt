package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable


data class ShortProject( //mozda ce trebat promijenit iz data class jer ima metodu (?)
    var id: Long = -1,
    var projectName: String = "",
    var hours: Int? = 0,
) : Serializable