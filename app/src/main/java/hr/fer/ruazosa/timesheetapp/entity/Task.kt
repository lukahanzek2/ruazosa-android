package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable
import java.util.*

data class Task(
    val id: Long? = null,
    var taskName: String = "",
    var taskDescription: String = "",
    var date: Date? = null, //ili mozda nekako namjestiti da bude default danasnji datum?
    var hours: Int = 0,
) : Serializable{
    override fun equals(other: Any?): Boolean {
        if(other == null){
            return false
        }
        if(other.javaClass != this.javaClass){
            return false
        }
        if((other as Task).id == this.id){
            return true
        }
        return false
    }
}