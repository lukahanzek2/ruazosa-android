package hr.fer.ruazosa.timesheetapp

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.*
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import hr.fer.ruazosa.timesheetapp.auth.AuthActivity
import hr.fer.ruazosa.timesheetapp.databinding.TaskEnterActivityBinding
import hr.fer.ruazosa.timesheetapp.entity.JWT.TaskJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.Project
import hr.fer.ruazosa.timesheetapp.entity.ShortProject
import hr.fer.ruazosa.timesheetapp.entity.Task
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository
import hr.fer.ruazosa.timesheetapp.utilities.DateUtilities.makeDateToString
import kotlinx.android.synthetic.main.task_enter_activity.*
import java.util.*


class TaskEnterActivity: AppCompatActivity(), RecyclerViewOnClickListener, NoItemsCallback{
    private lateinit var binding: TaskEnterActivityBinding
    private lateinit var adapter: TaskAdapter
    private lateinit var project: Project
    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var dateButton: Button
    private lateinit var selectedDate : Date
    private var filterText: String = ""

    override fun onRestart() {
        super.onRestart()
        LoadTasks().execute(project.id.toString(), UserRepository(applicationContext).getLoggedInUser().username)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TaskEnterActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        buildRecyclerView()

        initDatePicker()
        dateButton = binding.datePickerButton
        //posatviti default ---
        dateButton.setText("---")//dateButton.setText("          ---")

        //logika za ako je odabran datum koji je vise od tjedan dana prije danasnjeg, sakriti gumb za dodavanje zadataka, inace ga omoguciti
        binding.addNewTaskbtnId.setOnClickListener {
            val intent = Intent(applicationContext, TaskEditActivity::class.java)
            intent.putExtra("PROJECT_ID", project.id)
            intent.putExtra("NEW_TASK", true) //dodavanje zadatka
            if(dateButton.text.equals("---")){
                intent.putExtra("SELECTED_DATE", null as Date?) //null -> nije odabran niti jedan datum
            }
            else intent.putExtra("SELECTED_DATE", selectedDate)
            startActivity(intent)
        }

        setSupportActionBar(toolbarTask)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }


    private fun initDatePicker() {
        val dateSetListener = OnDateSetListener { datePicker, year, month, day ->
            selectedDate = Date(year - 1900, month, day)
            dateButton.text = makeDateToString(selectedDate)//dateButton.text = adapter.makeDateToString(selectedDate)
            adapter.getFilterDate().filter(selectedDate.toString()) //filtriraj zadatke po datumu
            //ako je odabran datum koji je vise od tjedan dana prije danasnjeg, sakriti gumb za dodavanje zadataka, inace ga omoguciti
            if(selectedDate.before(Date(System.currentTimeMillis() - 604800000))) binding.addNewTaskbtnId.visibility = View.INVISIBLE
            else binding.addNewTaskbtnId.visibility = View.VISIBLE
        }
        var cal = Calendar.getInstance()
        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH) + 1 // jer je JAN = 0
        var day = cal.get(Calendar.DAY_OF_MONTH)

        var style = AlertDialog.BUTTON_POSITIVE//var style = AlertDialog.THEME_HOLO_LIGHT
        datePickerDialog = DatePickerDialog(this, style, dateSetListener, year, month, day)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() //omogucava odabir samo danasnjeg i proslih datuma
        //datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 604800000 //604 800 000 7 dana u minisekundama
    }


    fun openDatePicker(view: View?) {
        datePickerDialog.show()
    }

    private fun buildRecyclerView() {
        // this creates a vertical layout Manager
        binding.tasksListViewId.layoutManager = LinearLayoutManager(this)

        //Ovako dohvaćamo ShortProject poslan od ProjecActivity-a na kojeg smo kliknuli
        val bundle = intent.extras
        val projectTemp = bundle!!.getSerializable("SHORT_PROJECT") as ShortProject
        project = Project()
        project.id = projectTemp.id!!
        project.projectName = projectTemp.projectName

        LoadTasks().execute(project.id.toString(), UserRepository(applicationContext).getLoggedInUser().username)
    }

    private inner class LoadTasks: AsyncTask<String, Void, Pair<TaskJWTResponse?, String?>>() {
        override fun doInBackground(vararg params: String): Pair<TaskJWTResponse?, String?> {
            val rest = RestFactory.getInstance(applicationContext)
            return rest.getlistOfTasks(params[0].toLong(), params[1])
        }

        override fun onPostExecute(returnedData: Pair<TaskJWTResponse?, String?>) {
            val jwtResponse = returnedData.first
            val message = returnedData.second
            if(jwtResponse?.statusCode == 401){
                Toast.makeText(applicationContext, "Session expired", Toast.LENGTH_LONG).show()
                UserRepository(applicationContext).clearPreferences()
                val intent = Intent(applicationContext, AuthActivity::class.java)
                startActivity(intent)
            }
            if(jwtResponse == null || jwtResponse.tasksList == null){
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            } else{
                //adapter mora biti inicijaliziran bez obzira ima li ili nema zadataka zato jer filter pristupa njegovoj listi -> ako nema zadataka inicijaliziramo adapter svejedno s praznom listom (funkcija fillProjectsWithTasks)
                if(jwtResponse.tasksList!![0] == null){
                    this@TaskEnterActivity.onRecyclerViewRefresh(true)
                }else{
                    this@TaskEnterActivity.onRecyclerViewRefresh(false)
                }
                project.tasks.clear()
                project = fillProjectWithTasks(project, jwtResponse.tasksList)
                adapter = TaskAdapter(project.tasks.asReversed()  as List<Task>, this@TaskEnterActivity, this@TaskEnterActivity)
                binding.tasksListViewId.adapter = adapter
                if(!binding.datePickerButton.text.equals("---")){
                    adapter.getFilterDate().filter(selectedDate.toString())
                }
                adapter.getFilterText().filter(filterText)
            }

        }
    }
    fun fillProjectWithTasks(project: Project, tasks: List<Task>?): Project{
        if(tasks == null || tasks[0] == null){
            project.tasks = ArrayList<Task>()
        }else{
            tasks!!.forEach{
                project.tasks!!.add(it)
            }
        }

        return project
    }

    override fun onClick(position: Int) {
        val intent = Intent(applicationContext, TaskEditActivity::class.java)
        val task = adapter.dataSet[position]
        intent.putExtra("PROJECT_ID", project.id)
        intent.putExtra("TASK", task) //koristimo kasnije za edit btn
        intent.putExtra("NEW_TASK", false) //uređivanje zadatka
        popUpWindow(task, intent)

    }

    private fun popUpWindow(task:Task, intent:Intent){
        // inflate the layout of the popup window
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewPopupWindow = inflater.inflate(R.layout.task_info_popup, null)

        // create the popup window
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it

        val popupWindow = PopupWindow(viewPopupWindow, width, height, focusable)

        //popuni polja za ispis
        val taskNameTv = popupWindow.contentView.findViewById<TextView>(R.id.taskNameId2)
        taskNameTv.setText(task.taskName)

        val taskDescriptionTv = popupWindow.contentView.findViewById<TextView>(R.id.taskDescriptionId2)
        taskDescriptionTv.setText(task.taskDescription)
        taskDescriptionTv.movementMethod = ScrollingMovementMethod()


        val taskDateTv = popupWindow.contentView.findViewById<TextView>(R.id.taskDateId2)
        taskDateTv.setText(makeDateToString(task.date))

        val taskHoursTv = popupWindow.contentView.findViewById<TextView>(R.id.taskHoursId2)
        taskHoursTv.setText(task.hours.toString())


        val editBtn = popupWindow.contentView.findViewById<Button>(R.id.eiditBtnId)

        if(task.date!!.before(Date(System.currentTimeMillis() - 604800000))) editBtn.visibility = View.INVISIBLE
        else editBtn.visibility = View.VISIBLE

        editBtn.setOnClickListener {
            startActivity(intent) //klikom na edit -> TaskEditActivity
            popupWindow.dismiss()
        }
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(viewPopupWindow, Gravity.CENTER, 0, 0)

        // dismiss the popup window when touched
        /*viewPopupWindow.setOnTouchListener { v, event -> //micemo tako da se popup moze iskljucit samo kad se dotakne izvan njega
            popupWindow.dismiss()
            true
        }*/

    }

    override fun onRecyclerViewRefresh(visible: Boolean) {
        if(visible) binding.EmptyRecyclerViewText.visibility = View.VISIBLE
        else binding.EmptyRecyclerViewText.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_icons_tasks, menu)

        val searchItem = menu?.findItem(R.id.actionSearch)
        val searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                searchView.setQuery("", false)
                searchItem.collapseActionView()
                Toast.makeText(this@TaskEnterActivity, "$query", Toast.LENGTH_LONG).show()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                filterText = newText?:""
                adapter.getFilterText().filter(filterText)

                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var itemview = item.itemId

        when(itemview){
            R.id.LogoutBtnTasks -> {
                val logoutIntent = Intent(this, AuthActivity::class.java)
                logoutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                UserRepository(applicationContext).clearPreferences()  //logout -> izbisi user-a iz lokalne baze
                startActivity(logoutIntent)
            }
        }
        return false
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}