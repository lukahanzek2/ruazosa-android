package hr.fer.ruazosa.timesheetapp.entity

import java.io.Serializable

data class ShortPerson(
    var username: String = "",
    var password: String = "",
) : Serializable {
    constructor(p: Person) : this(
        p.username!!,
        p.password!!
    )
}