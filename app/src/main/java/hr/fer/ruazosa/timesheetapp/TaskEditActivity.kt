package hr.fer.ruazosa.timesheetapp

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hr.fer.ruazosa.timesheetapp.auth.AuthActivity
import hr.fer.ruazosa.timesheetapp.databinding.TaskEditActivityBinding
import hr.fer.ruazosa.timesheetapp.entity.JWT.EmptyJWTResponse
import hr.fer.ruazosa.timesheetapp.entity.Task
import hr.fer.ruazosa.timesheetapp.net.RestFactory
import hr.fer.ruazosa.timesheetapp.repositories.UserRepository
import hr.fer.ruazosa.timesheetapp.utilities.DateUtilities.makeDateToString
import java.util.*

class TaskEditActivity : AppCompatActivity() {
    private lateinit var binding: TaskEditActivityBinding
    private var task: Task? = null
    private lateinit var dateButton: Button
    private lateinit var datePickerDialog: DatePickerDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = TaskEditActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        buildLayout()

        binding.discardbtnId.setOnClickListener {
            finish()
        }

        binding.savebtnId.setOnClickListener {
            val taskName = binding.taskNameId.text.toString()
            val taskDescription = binding.taskDescriptionId.text.toString()
            val taskDateString = binding.datePickerButton2.text.toString()
            val taskDate = Date(datePickerDialog.datePicker.year - 1900, datePickerDialog.datePicker.month, datePickerDialog.datePicker.dayOfMonth)
            val taskHours = binding.hoursId.text.toString()

            var message = fieldsValidMessage(taskName, taskDescription, taskDateString, taskHours)

            if(message.isNotEmpty()){
                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            }
            else {
                if(task == null){
                    saveChanges(Task(null, taskName, taskDescription, taskDate, taskHours.toInt()), true)//sprema izmjene u bazu
                }else{
                    saveChanges(Task(task!!.id, taskName, taskDescription, taskDate, taskHours.toInt()), false)//sprema izmjene u bazu
                }
            }
        }


    }

    private fun fieldsValidMessage(taskName: String, taskDescription: String, taskDateString: String, taskHours: String): String{
        var message = String()
        if(taskName.equals("")){
            message += "Task name required\n"
        }
        if(taskDescription.equals("")){
            message += "Description required\n"
        }
        if(taskDateString.equals("---")){
            message += "Date required\n"
        }
        if(taskHours.equals("")){
            message += "Hours required\n"
        }

        return message
    }

    private fun buildLayout() {
        //Ovako dohvaćamo Task poslan od TaskEnterActivityja na kojeg smo kliknuli
        task = intent.extras?.getSerializable("TASK") as Task?
        val selectedDate = intent.extras?.getSerializable("SELECTED_DATE") as Date?

        binding.taskNameId.setText(task?.taskName)
        binding.taskDescriptionId.setText(task?.taskDescription)
        binding.hoursId.setText(task?.hours?.toString())

        //taskDate
        initDatePicker()
        dateButton = binding.datePickerButton2

        dateButton.text = (makeDateToString(task?.date))

        //ako smo dosli pritiskom na PLUS, ostavljamo prenesenu vrijednost datuma koji je selektiran u prethodnoj aktivnosti
        if(task == null){
            if(selectedDate == null){
                dateButton.text = ("---") //nije bio odabran niti jedan datum
            } else {
                dateButton.text = makeDateToString(selectedDate)
                datePickerDialog.updateDate(selectedDate.year + 1900, selectedDate.month, selectedDate.date)
            }
        }else{
            datePickerDialog.updateDate(task!!.date!!.year + 1900 , task!!.date!!.month, task!!.date!!.date)
        }

    }

    private fun initDatePicker() {
        val dateSetListener = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            var selectedDate = Date(year - 1900, month, day)
            dateButton.text = makeDateToString(selectedDate)
        }
        var cal = Calendar.getInstance()
        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH)
        var day = cal.get(Calendar.DAY_OF_MONTH)

        var style = AlertDialog.THEME_HOLO_LIGHT
        datePickerDialog = DatePickerDialog(this, style, dateSetListener, year, month, day)
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() //omogucava odabir samo danasnjeg i proslih datuma
        datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 604800000 //604 800 000 7 dana u minisekundama, omogucava odabir 7 proslih dana od danasnjeg
    }

    fun openDatePicker(view: View?) {
        datePickerDialog.show()
    }

    private fun saveChanges(task : Task, newTask: Boolean) {
        val projectId: Long = intent.extras!!.get("PROJECT_ID") as Long
        if(newTask){
            AddTask().execute(task as Object, projectId as Object) //potreno proslijediti id projekta
        }else{
            EditTask().execute(task)
        }
    }
    private inner class AddTask: AsyncTask<Object, Void, Pair<EmptyJWTResponse?, String?>>() {
        override fun doInBackground(vararg params: Object): Pair<EmptyJWTResponse?, String?> {
            val task = params[0] as Task
            val projectId: Long = params[1] as Long
            val userName = UserRepository(applicationContext).getLoggedInUser().username!! //mozda treba maknut usklicnike

            val rest = RestFactory.getInstance(applicationContext)!!

            return rest.addTask(task, projectId, userName)
        }

        override fun onPostExecute(returnedData: Pair<EmptyJWTResponse?, String?>) {
            val jwtResponse = returnedData.first
            val message = returnedData.second
            if(jwtResponse?.statusCode == 401){
                Toast.makeText(applicationContext, "Session expired", Toast.LENGTH_LONG).show()
                UserRepository(applicationContext).clearPreferences()
                val intent = Intent(applicationContext, AuthActivity::class.java)
                startActivity(intent)
            }

            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            finish()
        }
    }

    private inner class EditTask: AsyncTask<Task, Void, Pair<EmptyJWTResponse?, String?>>() {
        override fun doInBackground(vararg params: Task): Pair<EmptyJWTResponse?, String?> {
            val task: Task = params[0]

            val rest = RestFactory.getInstance(applicationContext)

            return rest.editTask(task)
        }

        override fun onPostExecute(returnedData: Pair<EmptyJWTResponse?, String?>) {
            val jwtResponse = returnedData.first
            val message = returnedData.second
            if(jwtResponse?.statusCode == 401){
                Toast.makeText(applicationContext, "Session expired", Toast.LENGTH_LONG).show()
                UserRepository(applicationContext).clearPreferences()
                val intent = Intent(applicationContext, AuthActivity::class.java)
                startActivity(intent)
            }

            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
            finish()
        }
    }

}