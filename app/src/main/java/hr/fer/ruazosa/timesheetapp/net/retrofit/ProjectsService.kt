package hr.fer.ruazosa.timesheetapp.net.retrofit

import hr.fer.ruazosa.timesheetapp.entity.Project
import hr.fer.ruazosa.timesheetapp.entity.ShortProject
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.Call

interface ProjectsService {
    @GET("/projects/{username}")
    fun listOfProjects(@Path("username") username: String?): Call<List<ShortProject>>

}